/*
 * 
 */
package tabs.hes.vm;

/**
 * Class Definition
 *
 * @author Tab Solutions
 */
public class VMeter {

    public static void main(String[] args) throws Exception {

        int jumlahPort = Integer.parseInt(args[0]);
        int numberDiff = Integer.parseInt(args[1]);

        for (int i = 0; i < jumlahPort; i++) {
            String nomorMeter;
            if (i < 10) {
                nomorMeter = "15" + numberDiff + "000000000" + i;
            } else if (i < 100) {
                nomorMeter = "15" + numberDiff + "00000000" + i;
            } else if (i < 1000) {
                nomorMeter = "15" + numberDiff + "0000000" + i;
            } else if (i < 10000) {
                nomorMeter = "15" + numberDiff + "000000" + i;
            } else if (i < 100000) {
                nomorMeter = "15" + numberDiff + "00000" + i;
            } else if (i < 1000000) {
                nomorMeter = "15" + numberDiff + "0000" + i;
            } else if (i < 10000000) {
                nomorMeter = "15" + numberDiff + "000" + i;
            } else {
                nomorMeter = "15" + numberDiff + "00" + i;
            }
            StartClass.startNewMeter((10000 + i), nomorMeter);
        }
        System.out.println("Press Enter to close.");
        while (System.in.read() != 10) {
            System.out.println("Press Enter to close.");
        }
        /// Close servers.
        System.out.println("Server closed.");
        System.exit(0);
    }
}
