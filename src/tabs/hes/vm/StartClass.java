/*
 * 
 */

package tabs.hes.vm;

import tabs.hes.vm.server.TabsDLMSServerLN;

/**
 * Class Definition
 *
 * @author Tab Solutions
 */
public class StartClass {

    public static void startNewMeter(int port, String nomorMeter) throws Exception {
        TabsDLMSServerLN LNServer = new TabsDLMSServerLN(nomorMeter);
        LNServer.initialize(port);
        System.out.println(
                "----------------------------------------------------------");
    }
}
