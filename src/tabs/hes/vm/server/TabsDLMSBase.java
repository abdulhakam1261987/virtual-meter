//
// --------------------------------------------------------------------------
//  Gurux Ltd
// 
//
//
// Filename:        $HeadURL:  $
//
// Version:         $Revision: $,
//                  $Date:  $
//                  $Author: $
//
// Copyright (c) Gurux Ltd
//
//---------------------------------------------------------------------------
//
//  DESCRIPTION
//
// This file is a part of Gurux Device Framework.
//
// Gurux Device Framework is Open Source software; you can redistribute it
// and/or modify it under the terms of the GNU General Public License 
// as published by the Free Software Foundation; version 2 of the License.
// Gurux Device Framework is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
// See the GNU General Public License for more details.
//
// More information of Gurux DLMS/COSEM Director: https://www.gurux.org/GXDLMSDirector
//
// This code is licensed under the GNU General Public License v2. 
// Full text may be retrieved at http://www.gnu.org/licenses/gpl-2.0.txt
//---------------------------------------------------------------------------
package tabs.hes.vm.server;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import gurux.common.GXCommon;
import gurux.common.IGXMedia;
import gurux.common.IGXMediaListener;
import gurux.common.MediaStateEventArgs;
import gurux.common.PropertyChangedEventArgs;
import gurux.common.ReceiveEventArgs;
import gurux.common.TraceEventArgs;
import gurux.common.enums.TraceLevel;
import gurux.dlms.GXDLMSClient;
import gurux.dlms.GXDLMSConnectionEventArgs;
import gurux.dlms.GXDLMSTranslator;
import gurux.dlms.GXDateTime;
import gurux.dlms.GXServerReply;
import gurux.dlms.GXSimpleEntry;
import gurux.dlms.GXTime;
import gurux.dlms.ValueEventArgs;
import gurux.dlms.enums.AccessMode;
import gurux.dlms.enums.Authentication;
import gurux.dlms.enums.DataType;
import gurux.dlms.enums.MethodAccessMode;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.enums.Security;
import gurux.dlms.enums.SourceDiagnostic;
import gurux.dlms.enums.Unit;
import gurux.dlms.objects.GXDLMSActionSchedule;
import gurux.dlms.objects.GXDLMSActivityCalendar;
import gurux.dlms.objects.GXDLMSAssociationLogicalName;
import gurux.dlms.objects.GXDLMSAssociationShortName;
import gurux.dlms.objects.GXDLMSAutoAnswer;
import gurux.dlms.objects.GXDLMSAutoConnect;
import gurux.dlms.objects.GXDLMSCaptureObject;
import gurux.dlms.objects.GXDLMSClock;
import gurux.dlms.objects.GXDLMSCompactData;
import gurux.dlms.objects.GXDLMSData;
import gurux.dlms.objects.GXDLMSDayProfile;
import gurux.dlms.objects.GXDLMSDayProfileAction;
import gurux.dlms.objects.GXDLMSDemandRegister;
import gurux.dlms.objects.GXDLMSDisconnectControl;
import gurux.dlms.objects.GXDLMSGSMDiagnostic;
import gurux.dlms.objects.GXDLMSHdlcSetup;
import gurux.dlms.objects.GXDLMSIECOpticalPortSetup;
import gurux.dlms.objects.GXDLMSImageTransfer;
import gurux.dlms.objects.GXDLMSIp4Setup;
import gurux.dlms.objects.GXDLMSIp6Setup;
import gurux.dlms.objects.GXDLMSMacAddressSetup;
import gurux.dlms.objects.GXDLMSModemConfiguration;
import gurux.dlms.objects.GXDLMSModemInitialisation;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.dlms.objects.GXDLMSPushSetup;
import gurux.dlms.objects.GXDLMSRegister;
import gurux.dlms.objects.GXDLMSRegisterMonitor;
import gurux.dlms.objects.GXDLMSSapAssignment;
import gurux.dlms.objects.GXDLMSScript;
import gurux.dlms.objects.GXDLMSScriptAction;
import gurux.dlms.objects.GXDLMSScriptTable;
import gurux.dlms.objects.GXDLMSSeasonProfile;
import gurux.dlms.objects.GXDLMSSecuritySetup;
import gurux.dlms.objects.GXDLMSTcpUdpSetup;
import gurux.dlms.objects.GXDLMSWeekProfile;
import gurux.dlms.objects.enums.AutoAnswerMode;
import gurux.dlms.objects.enums.AutoAnswerStatus;
import gurux.dlms.objects.enums.AutoConnectMode;
import gurux.dlms.objects.enums.BaudRate;
import gurux.dlms.objects.enums.LocalPortResponseTime;
import gurux.dlms.objects.enums.OpticalProtocolMode;
import gurux.dlms.objects.enums.SingleActionScheduleType;
import gurux.dlms.objects.enums.SortMethod;
import gurux.dlms.secure.GXDLMSSecureServer2;
import gurux.io.Parity;
import gurux.io.StopBits;
import gurux.net.GXNet;
import gurux.net.enums.NetworkType;
import gurux.serial.GXSerial;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * All example servers are using same objects.
 */
public class TabsDLMSBase extends GXDLMSSecureServer2
        implements IGXMediaListener, gurux.net.IGXNetListener {

    private IGXMedia media;

    private final String nomorMeter;

    private Random random = new Random();

    static final Object fileLPLock = new Object();

    static final Object fileEOBLock = new Object();

    private Calendar lastLPCalendar = Calendar.getInstance();

    private int lastLPPos = 0;

    private Calendar lastEOBCalendar = Calendar.getInstance();

    private int lastEOBPos = 0;

    // Date file is saved to same directory where app is.
    final String getDataLPFile() {
        final String dir = Paths
                .get(TabsDLMSBase.class.getProtectionDomain().getCodeSource()
                        .getLocation().getPath().substring(1))
                .getParent().toString();
        return dir + "/data" + nomorMeter + "LP.csv";
    }

    final String getDataEOBFile() {
        final String dir = Paths
                .get(TabsDLMSBase.class.getProtectionDomain().getCodeSource()
                        .getLocation().getPath().substring(1))
                .getParent().toString();
        return dir + "/data" + nomorMeter + "EOB.csv";
    }

    /**
     * Constructor.
     *
     * @param ln Association logical name.
     * @param hdlc HDLC settings.
     */
    public TabsDLMSBase(final GXDLMSAssociationLogicalName ln,
            final GXDLMSHdlcSetup hdlc, String nomorMeter) {
        super(ln, hdlc);
        this.nomorMeter = nomorMeter;
        this.setMaxReceivePDUSize(1024);
        byte[] secret = "Tab".getBytes();
        ln.setSecret(secret);
        // Add security setup object.
        ln.setSecuritySetupReference("0.0.43.0.0.255");
        GXDLMSSecuritySetup s = new GXDLMSSecuritySetup();
        s.setServerSystemTitle(getCiphering().getSystemTitle());
        getItems().add(s);
    }

    /*
     * Add Logical Device Name. 123456 is meter serial number.
     */
    void addLogicalDeviceName() {
        GXDLMSData d = new GXDLMSData("0.0.42.0.0.255");
        d.setValue("Tab Virtual Meter 1.0");
        // Set access right. Client can't change Device name.
        d.setAccess(2, AccessMode.READ);
        d.setDataType(2, DataType.OCTET_STRING);
        d.setUIDataType(2, DataType.STRING);
        getItems().add(d);
    }

    GXDLMSData getMeterNumber() {
        GXDLMSData d = new GXDLMSData("0.0.96.1.0.255");
        d.setValue(nomorMeter);
        // Set access right. Client can't change Device name.
        d.setAccess(2, AccessMode.READ);
        d.setDataType(2, DataType.OCTET_STRING);
        d.setUIDataType(2, DataType.STRING);
        return d;
    }

    /*
     * Add meter serial number. 1123581321 is meter serial number.
     */
    void addMeterNumber() {
        GXDLMSData d = new GXDLMSData("0.0.96.1.0.255");
        d.setValue(nomorMeter);
        // Set access right. Client can't change Device name.
        d.setAccess(2, AccessMode.READ);
        d.setDataType(2, DataType.OCTET_STRING);
        d.setUIDataType(2, DataType.STRING);
        getItems().add(d);
    }

    /*
     * Add meter ID 1.
     */
    void addMeterID1() {
        GXDLMSData d = new GXDLMSData("0.0.96.1.1.255");
        d.setValue(nomorMeter + "00" + "1");
        // Set access right. Client can't change Device name.
        d.setAccess(2, AccessMode.READ_WRITE);
        d.setDataType(2, DataType.OCTET_STRING);
        d.setUIDataType(2, DataType.STRING);
        getItems().add(d);
    }

    /*
     * Add meter ID 2.
     */
    void addMeterID2() {
        GXDLMSData d = new GXDLMSData("0.0.96.1.2.255");
        d.setValue(nomorMeter + "00" + "2");
        // Set access right. Client can't change Device name.
        d.setAccess(2, AccessMode.READ_WRITE);
        d.setDataType(2, DataType.OCTET_STRING);
        d.setUIDataType(2, DataType.STRING);
        getItems().add(d);
    }

    /*
     * Add meter ID 3.
     */
    void addMeterID3() {
        GXDLMSData d = new GXDLMSData("0.0.96.1.3.255");
        d.setValue(nomorMeter + "00" + "3");
        // Set access right. Client can't change Device name.
        d.setAccess(2, AccessMode.READ_WRITE);
        d.setDataType(2, DataType.OCTET_STRING);
        d.setUIDataType(2, DataType.STRING);
        getItems().add(d);
    }

    /*
     * Add meter ID 4.
     */
    void addMeterID4() {
        GXDLMSData d = new GXDLMSData("0.0.96.1.4.255");
        d.setValue(nomorMeter + "00" + "4");
        // Set access right. Client can't change Device name.
        d.setAccess(2, AccessMode.READ_WRITE);
        d.setDataType(2, DataType.OCTET_STRING);
        d.setUIDataType(2, DataType.STRING);
        getItems().add(d);
    }

    /*
     * Add meter ID 5.
     */
    void addMeterID5() {
        GXDLMSData d = new GXDLMSData("0.0.96.1.5.255");
        d.setValue(nomorMeter + "00" + "5");
        // Set access right. Client can't change Device name.
        d.setAccess(2, AccessMode.READ_WRITE);
        d.setDataType(2, DataType.OCTET_STRING);
        d.setUIDataType(2, DataType.STRING);
        getItems().add(d);
    }

    /*
     * Add meter ID 6.
     */
    void addMeterID6() {
        GXDLMSData d = new GXDLMSData("0.0.96.1.6.255");
        d.setValue(nomorMeter + "00" + "6");
        // Set access right. Client can't change Device name.
        d.setAccess(2, AccessMode.READ_WRITE);
        d.setDataType(2, DataType.OCTET_STRING);
        d.setUIDataType(2, DataType.STRING);
        getItems().add(d);
    }

    /*
     * Add meter ID 7.
     */
    void addMeterID7() {
        GXDLMSData d = new GXDLMSData("0.0.96.1.7.255");
        d.setValue(nomorMeter + "00" + "7");
        // Set access right. Client can't change Device name.
        d.setAccess(2, AccessMode.READ_WRITE);
        d.setDataType(2, DataType.OCTET_STRING);
        d.setUIDataType(2, DataType.STRING);
        getItems().add(d);
    }

    /*
     * Add meter ID 8.
     */
    void addMeterID8() {
        GXDLMSData d = new GXDLMSData("0.0.96.1.8.255");
        d.setValue(nomorMeter + "00" + "8");
        // Set access right. Client can't change Device name.
        d.setAccess(2, AccessMode.READ_WRITE);
        d.setDataType(2, DataType.OCTET_STRING);
        d.setUIDataType(2, DataType.STRING);
        getItems().add(d);
    }

    /*
     * Add meter ID 8.
     */
    void addMeterID9() {
        GXDLMSData d = new GXDLMSData("0.0.96.1.9.255");
        d.setValue(nomorMeter + "00" + "9");
        // Set access right. Client can't change Device name.
        d.setAccess(2, AccessMode.READ_WRITE);
        d.setDataType(2, DataType.OCTET_STRING);
        d.setUIDataType(2, DataType.STRING);
        getItems().add(d);
    }

    /*
     * Add firmware version.
     */
    void addFirmwareVersion() {
        GXDLMSData d = new GXDLMSData("1.0.0.2.0.255");
        d.setValue("Tab Virtual Meter 1.0");
        getItems().add(d);
    }

    /*
     * Add invocation counter.
     */
    void addInvocationCounter() {
        GXDLMSData d = new GXDLMSData("0.0.43.1.0.255");
        d.setValue(0);
        d.setDataType(2, DataType.UINT32);
        // Set initial value.
        d.setValue(100);
        getItems().add(d);
    }

    GXDLMSRegister addRegister() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.1.21.25.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addTeganganR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.32.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(110) + 110);
        r.setScaler(1);
        r.setUnit(Unit.VOLTAGE);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addArusR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.31.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(100));
        r.setScaler(0.1);
        r.setUnit(Unit.CURRENT);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addArusN() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.91.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(10));
        r.setScaler(1);
        r.setUnit(Unit.CURRENT);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addPFR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.33.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(100));
        r.setScaler(0.01);
        r.setUnit(Unit.NO_UNIT);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addDayaAktifImpR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.21.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addDayaAktifExpR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.22.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addDayaReaktifImpR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.23.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addDayaReaktifExpR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.24.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addDayaApparentExpR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.29.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.APPARENT_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addDayaApparentImpR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.30.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.APPARENT_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiAktifImpR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.21.8.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiAktifExpR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.22.8.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiReaktifImpR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.23.8.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiReaktifExpR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.24.8.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiApparentExpR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.29.8.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.APPARENT_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addMaksApparentExpTotal() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.9.6.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.APPARENT_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addTotalKejadianMeterOff() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("0.1.96.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(30));
        r.setScaler(1);
        r.setUnit(Unit.NO_UNIT);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addTotalKejadianTamper() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("0.1.96.20.46.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(30));
        r.setScaler(1);
        r.setUnit(Unit.NO_UNIT);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addKapasitasBaterai() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("0.0.96.6.3.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(30));
        r.setScaler(1);
        r.setUnit(Unit.AMPERE_HOURS);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiApparentImpR() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.30.8.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.APPARENT_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addDayaAktifImpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.1.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addDayaAktifExpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.2.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiAktifExpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.2.8.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiAktifExpWBP() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.2.8.1.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addPemakaianEnergiAktifExpWBP() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.2.9.1.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addPemakaianEnergiAktifExpLWBP1() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.2.9.2.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addPemakaianEnergiAktifExpLWBP2() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.2.9.3.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiAktifExpLWBP1() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.2.8.2.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiAktifExpLWBP2() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.2.8.3.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addPemakaianEnergiAktifExpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.2.9.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addDayaReaktifImpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.3.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiReaktifImpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.3.8.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addPemakaianEnergiReaktifImpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.3.9.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addDayaReaktifExpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.4.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiReaktifExpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.4.8.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiReaktifBilling() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.128.8.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addPemakaianEnergiReaktifExpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.4.9.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addDayaApparentImpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.9.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.APPARENT_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addDayaApparentExpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.10.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.APPARENT_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addArusTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.11.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(10));
        r.setScaler(0.01);
        r.setUnit(Unit.CURRENT);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addTeganganTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.12.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(110) + 110);
        r.setScaler(0.1);
        r.setUnit(Unit.VOLTAGE);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addPowerFactorTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.13.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(100));
        r.setScaler(0.01);
        r.setUnit(Unit.NO_UNIT);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addActivePowerAbsPlusTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.15.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(10000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addActivePowerAbsMinTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.16.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(10000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addReactivePowerQ1Tot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.5.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addReactivePowerQ2Tot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.6.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addReactivePowerQ3Tot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.7.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addReactivePowerQ4Tot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.8.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.REACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addActivePowerQ1Tot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.17.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addActivePowerQ2Tot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.18.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addActivePowerQ3Tot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.19.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addActivePowerQ4Tot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.20.7.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiAbsAktifPowerMin() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.16.8.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiAktifImpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.1.8.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiAktifImpWBP() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.1.8.1.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addPemakaianEnergiAktifImpWBP() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.1.9.1.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addPemakaianEnergiAktifImpLWBP1() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.1.9.2.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addPemakaianEnergiAktifImpLWBP2() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.1.9.3.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiAktifImpLWBP1() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.1.8.2.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addStandEnergiAktifImpLWBP2() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.1.8.3.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    GXDLMSRegister addPemakaianEnergiAktifImpTot() {
        // Add Last average.
        GXDLMSRegister r = new GXDLMSRegister("1.0.1.9.0.255");
        // Set access right. Client can't change Device name.
        r.setAccess(2, AccessMode.READ);
        r.setValue(random.nextInt(1000));
        r.setScaler(1);
        r.setUnit(Unit.ACTIVE_POWER);
        getItems().add(r);
        return r;
    }

    /**
     * Add default clock. Clock's Logical Name is 0.0.1.0.0.255.
     */
    GXDLMSClock addClock() {
        GXDLMSClock clock = new GXDLMSClock();
        clock.setBegin(new GXDateTime(-1, 9, 1, -1, -1, -1, -1));
        clock.setEnd(new GXDateTime(-1, 3, 1, -1, -1, -1, -1));
        clock.setDeviation(0);
        getItems().add(clock);
        return clock;
    }

    /**
     * Add TCP/IP UDP setup object.
     */
    void addTcpUdpSetup() {
        GXDLMSTcpUdpSetup tcp = new GXDLMSTcpUdpSetup();
        getItems().add(tcp);
    }

    /*
     * Add load profile (historical) object.
     */
    GXDLMSProfileGeneric addLoadProfile() {
        GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric("1.0.99.1.0.255");
        GXDLMSClock clock = new GXDLMSClock();
        clock.setTime(Calendar.getInstance());
        GXDLMSRegister tegR = new GXDLMSRegister("1.0.32.7.0.255");
        GXDLMSRegister arusR = new GXDLMSRegister("1.0.31.7.0.255");
        GXDLMSRegister arusN = new GXDLMSRegister("1.0.91.7.0.255");
        GXDLMSRegister pFR = new GXDLMSRegister("1.0.33.7.0.255");
        GXDLMSRegister dayaAktifImpR = new GXDLMSRegister("1.0.21.7.0.255");
        GXDLMSRegister dayaAktifExpR = new GXDLMSRegister("1.0.22.7.0.255");
        GXDLMSRegister dayaReaktifImpR = new GXDLMSRegister("1.0.23.7.0.255");
        GXDLMSRegister dayaReaktifExpR = new GXDLMSRegister("1.0.24.7.0.255");
        GXDLMSRegister dayaApparentImpR = new GXDLMSRegister("1.0.29.7.0.255");
        GXDLMSRegister dayaApparentExpR = new GXDLMSRegister("1.0.30.7.0.255");
        GXDLMSRegister standEnergiAktifImpR = new GXDLMSRegister("1.0.21.8.0.255");
        GXDLMSRegister standEnergiAktifExpR = new GXDLMSRegister("1.0.22.8.0.255");
        GXDLMSRegister standEnergiReaktifImpR = new GXDLMSRegister("1.0.23.8.0.255");
        GXDLMSRegister standEnergiReaktifExpR = new GXDLMSRegister("1.0.24.8.0.255");
        GXDLMSRegister standEnergiApparentImpR = new GXDLMSRegister("1.0.29.8.0.255");
        GXDLMSRegister standEnergiApparentExpR = new GXDLMSRegister("1.0.30.8.0.255");
        // Set capture period to 60 second.
        pg.setCapturePeriod(60);
        // Maximum row count.
        pg.setProfileEntries(3000);
        pg.setSortMethod(SortMethod.FIFO);
        pg.setSortObject(clock);
        pg.addCaptureObject(clock, 2, 0);
        pg.addCaptureObject(tegR, 2, 0);
        pg.addCaptureObject(arusR, 2, 0);
        pg.addCaptureObject(arusN, 2, 0);
        pg.addCaptureObject(pFR, 2, 0);
        pg.addCaptureObject(dayaAktifImpR, 2, 0);
        pg.addCaptureObject(dayaAktifExpR, 2, 0);
        pg.addCaptureObject(dayaReaktifImpR, 2, 0);
        pg.addCaptureObject(dayaReaktifExpR, 2, 0);
        pg.addCaptureObject(dayaApparentImpR, 2, 0);
        pg.addCaptureObject(dayaApparentExpR, 2, 0);
        pg.addCaptureObject(standEnergiAktifImpR, 2, 0);
        pg.addCaptureObject(standEnergiAktifExpR, 2, 0);
        pg.addCaptureObject(standEnergiReaktifImpR, 2, 0);
        pg.addCaptureObject(standEnergiReaktifExpR, 2, 0);
        pg.addCaptureObject(standEnergiApparentImpR, 2, 0);
        pg.addCaptureObject(standEnergiApparentExpR, 2, 0);
        getItems().add(pg);
//         Create 10 000 rows for profile generic file.
        // In example profile generic we have two columns.
        // Date time and integer value.
        int rowCount = (Long.parseLong(nomorMeter) % 2 == 0) ? 2880 : random.nextInt(3000);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR_OF_DAY, -(rowCount - 1));
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat df = new SimpleDateFormat();

        for (int pos = 0; pos != rowCount; ++pos) {
            sb.append(df.format(cal.getTime()));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 220 : random.nextInt(110) + 110));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 69 : random.nextInt(100)));
            sb.append(';');
            sb.append(String.valueOf(0));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 1 : random.nextInt(100)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 835 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 123 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 450 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 367 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 532 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 284 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (835 * (pos + 1)) : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (123 * (pos + 1)) : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (450 * (pos + 1)) : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (367 * (pos + 1)) : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (532 * (pos + 1)) : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (284 * (pos + 1)) : random.nextInt(1000)));
            sb.append(System.lineSeparator());
            cal.add(Calendar.HOUR_OF_DAY, 1);
            lastLPPos = pos;
            lastLPCalendar = cal;
        }
        synchronized (fileLPLock) {
            FileWriter writer = null;
            try {
                writer = new FileWriter(getDataLPFile(), false);
                writer.write(sb.toString());
                writer.close();
            } catch (IOException e) {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e1) {
                    }
                }
                throw new RuntimeException(e.getMessage());
            }
        }
        lastLPCalendar.add(Calendar.HOUR_OF_DAY, -1);
        return pg;
    }

    /*
     * Add load profile (historical) object.
     */
    GXDLMSProfileGeneric addEOB() {
        GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric("1.0.98.1.0.255");
        GXDLMSClock clock = new GXDLMSClock();
        clock.setTime(Calendar.getInstance());
        GXDLMSData meterID = new GXDLMSData("0.0.96.1.0.255");
        GXDLMSRegister standEnergiAbsAktifPowerMin = new GXDLMSRegister("1.0.16.8.0.255");
        GXDLMSRegister standEnergiAktifImpTot = new GXDLMSRegister("1.0.1.8.0.255");
        GXDLMSRegister pemakaianEnergiAktifImpTot = new GXDLMSRegister("1.0.1.9.0.255");
        GXDLMSRegister standEnergiAktifExpTot = new GXDLMSRegister("1.0.2.8.0.255");
        GXDLMSRegister pemakaianEnergiAktifExpTot = new GXDLMSRegister("1.0.2.9.0.255");
        GXDLMSRegister standEnergiReaktifImpTot = new GXDLMSRegister("1.0.3.8.0.255");
        GXDLMSRegister pemakaianEnergiReaktifImpTot = new GXDLMSRegister("1.0.3.9.0.255");
        GXDLMSRegister standEnergiReaktifExpTot = new GXDLMSRegister("1.0.4.8.0.255");
        GXDLMSRegister pemakaianEnergiReaktifExpTot = new GXDLMSRegister("1.0.4.9.0.255");
        GXDLMSRegister standEnergiReaktifBilling = new GXDLMSRegister("1.0.128.8.0.255");
        GXDLMSRegister standEnergiAktifExpWBP = new GXDLMSRegister("1.0.2.8.1.255");
        GXDLMSRegister standEnergiAktifImpWBP = new GXDLMSRegister("1.0.1.8.1.255");
        GXDLMSRegister standEnergiAktifExpLWBP1 = new GXDLMSRegister("1.0.2.8.2.255");
        GXDLMSRegister standEnergiAktifImpLWBP1 = new GXDLMSRegister("1.0.1.8.2.255");
        GXDLMSRegister standEnergiAktifExpLWBP2 = new GXDLMSRegister("1.0.2.8.3.255");
        GXDLMSRegister standEnergiAktifImpLWBP2 = new GXDLMSRegister("1.0.1.8.3.255");
        GXDLMSRegister pemakaianEnergiAktifExpWBP = new GXDLMSRegister("1.0.2.9.1.255");
        GXDLMSRegister pemakaianEnergiAktifImpWBP = new GXDLMSRegister("1.0.1.9.1.255");
        GXDLMSRegister pemakaianEnergiAktifExpLWBP1 = new GXDLMSRegister("1.0.2.9.2.255");
        GXDLMSRegister pemakaianEnergiAktifImpLWBP1 = new GXDLMSRegister("1.0.1.9.2.255");
        GXDLMSRegister pemakaianEnergiAktifExpLWBP2 = new GXDLMSRegister("1.0.2.9.3.255");
        GXDLMSRegister pemakaianEnergiAktifImpLWBP2 = new GXDLMSRegister("1.0.1.9.3.255");
        GXDLMSRegister maksApparentExpTotal = new GXDLMSRegister("1.0.9.6.0.255");
        GXDLMSRegister totalKejadianMeterOff = new GXDLMSRegister("0.1.96.7.0.255");
        GXDLMSRegister totalKejadianTamper = new GXDLMSRegister("0.1.96.20.46.255");
        GXDLMSRegister kapasitasBaterai = new GXDLMSRegister("0.0.96.6.3.255");
        // Set capture period to 60 second.
        pg.setCapturePeriod(43200);
        // Maximum row count.
        pg.setProfileEntries(12);
        pg.setSortMethod(SortMethod.FIFO);
        pg.setSortObject(clock);
        pg.addCaptureObject(clock, 2, 0);
        pg.addCaptureObject(meterID, 2, 0);
        pg.addCaptureObject(standEnergiAbsAktifPowerMin, 2, 0);
        pg.addCaptureObject(standEnergiAktifImpTot, 2, 0);
        pg.addCaptureObject(pemakaianEnergiAktifImpTot, 2, 0);
        pg.addCaptureObject(standEnergiAktifExpTot, 2, 0);
        pg.addCaptureObject(pemakaianEnergiAktifExpTot, 2, 0);
        pg.addCaptureObject(standEnergiReaktifImpTot, 2, 0);
        pg.addCaptureObject(pemakaianEnergiReaktifImpTot, 2, 0);
        pg.addCaptureObject(standEnergiReaktifExpTot, 2, 0);
        pg.addCaptureObject(pemakaianEnergiReaktifExpTot, 2, 0);
        pg.addCaptureObject(standEnergiReaktifBilling, 2, 0);
        pg.addCaptureObject(standEnergiAktifExpWBP, 2, 0);
        pg.addCaptureObject(standEnergiAktifImpWBP, 2, 0);
        pg.addCaptureObject(standEnergiAktifExpLWBP1, 2, 0);
        pg.addCaptureObject(standEnergiAktifImpLWBP1, 2, 0);
        pg.addCaptureObject(standEnergiAktifExpLWBP2, 2, 0);
        pg.addCaptureObject(standEnergiAktifImpLWBP2, 2, 0);
        pg.addCaptureObject(pemakaianEnergiAktifExpWBP, 2, 0);
        pg.addCaptureObject(pemakaianEnergiAktifImpWBP, 2, 0);
        pg.addCaptureObject(pemakaianEnergiAktifExpLWBP1, 2, 0);
        pg.addCaptureObject(pemakaianEnergiAktifImpLWBP1, 2, 0);
        pg.addCaptureObject(pemakaianEnergiAktifExpLWBP2, 2, 0);
        pg.addCaptureObject(pemakaianEnergiAktifImpLWBP2, 2, 0);
        pg.addCaptureObject(maksApparentExpTotal, 2, 0);
        pg.addCaptureObject(totalKejadianMeterOff, 2, 0);
        pg.addCaptureObject(totalKejadianTamper, 2, 0);
        pg.addCaptureObject(kapasitasBaterai, 2, 0);
        getItems().add(pg);
//         Create 10 000 rows for profile generic file.
        // In example profile generic we have two columns.
        // Date time and integer value.
        int rowCount = (Long.parseLong(nomorMeter) % 2 == 0) ? 8 : random.nextInt(12);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, 1);
        cal.add(Calendar.MONTH, -(rowCount - 1));
        cal.set(Calendar.HOUR_OF_DAY, 1);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat df = new SimpleDateFormat();
        for (int pos = 0; pos != rowCount; ++pos) {
            sb.append(df.format(cal.getTime()));
            sb.append(';');
            sb.append(nomorMeter);
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (163 * (pos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (223 * (pos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 223 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (98 * (pos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 98 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (433 * (pos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 433 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (256 * (pos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 256 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (659 * (pos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (251 * (pos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (854 * (pos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (598 * (pos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (111 * (pos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (222 * (pos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (328 * (pos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 744 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 476 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 481 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 397 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 188 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 363 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 444 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 11 : random.nextInt(30)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (12 * (pos + 1)) : random.nextInt(30)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 2000 : random.nextInt(10000)));
            sb.append(System.lineSeparator());
            cal.add(Calendar.MONTH, 1);
            lastEOBCalendar = cal;
            lastEOBPos = pos;
        }
        synchronized (fileEOBLock) {
            FileWriter writer = null;
            try {
                writer = new FileWriter(getDataEOBFile(), false);
                writer.write(sb.toString());
                writer.close();
            } catch (IOException e) {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e1) {
                    }
                }
                throw new RuntimeException(e.getMessage());
            }
        }
        cal.add(Calendar.MONTH, -1);
        return pg;
    }

    /*
     * Add Auto connect object.
     */
    void addAutoConnect() {
        GXDLMSAutoConnect ac = new GXDLMSAutoConnect();
        ac.setMode(AutoConnectMode.AUTO_DIALLING_ALLOWED_ANYTIME);
        ac.setRepetitions(10);
        ac.setRepetitionDelay(60);
        // Calling is allowed between 1am to 6am.
        ac.getCallingWindow()
                .add(new AbstractMap.SimpleEntry<GXDateTime, GXDateTime>(
                        new GXDateTime(-1, -1, -1, 1, 0, 0, -1),
                        new GXDateTime(-1, -1, -1, 6, 0, 0, -1)));
        ac.setDestinations(new String[]{"www.gurux.org"});
        getItems().add(ac);
    }

    /*
     * Add Activity Calendar object.
     */
    void addActivityCalendar() {
        java.util.Calendar tm = Calendar.getInstance();
        java.util.Date now = tm.getTime();
        GXDLMSActivityCalendar activity = new GXDLMSActivityCalendar();
        activity.setCalendarNameActive("Active");
        activity.setSeasonProfileActive(new GXDLMSSeasonProfile[]{
            new GXDLMSSeasonProfile("Summer time",
            new GXDateTime(-1, -1, -1, -1, -1, 3, 31), "")});
        GXDLMSWeekProfile wp = new GXDLMSWeekProfile();
        wp.setName("Monday");
        wp.setMonday(1);
        wp.setTuesday(1);
        wp.setWednesday(1);
        wp.setThursday(1);
        wp.setFriday(1);
        wp.setSaturday(1);
        wp.setSunday(1);
        activity.setWeekProfileTableActive(new GXDLMSWeekProfile[]{wp});
        activity.setDayProfileTableActive(
                new GXDLMSDayProfile[]{new GXDLMSDayProfile(1,
                            new GXDLMSDayProfileAction[]{
                                new GXDLMSDayProfileAction(new GXTime(now),
                                        "0.1.10.1.101.255", 1)})});
        activity.setCalendarNamePassive("Passive");
        activity.setSeasonProfilePassive(new GXDLMSSeasonProfile[]{
            new GXDLMSSeasonProfile("Winter time",
            new GXDateTime(-1, -1, -1, -1, -1, 10, 30), "")});
        wp = new GXDLMSWeekProfile();
        wp.setName("Tuesday");
        wp.setMonday(1);
        wp.setTuesday(1);
        wp.setWednesday(1);
        wp.setThursday(1);
        wp.setFriday(1);
        wp.setSaturday(1);
        wp.setSunday(1);
        activity.setWeekProfileTablePassive(new GXDLMSWeekProfile[]{wp});
        activity.setDayProfileTablePassive(
                new GXDLMSDayProfile[]{new GXDLMSDayProfile(1,
                            new GXDLMSDayProfileAction[]{
                                new GXDLMSDayProfileAction(new GXTime(now),
                                        "0.0.1.0.0.255", 1)})});
        activity.setTime(new GXDateTime(now));
        getItems().add(activity);

        getItems().add(new GXDLMSIp6Setup());
    }

    /*
     * Add Optical Port Setup object.
     */
    void addOpticalPortSetup() {
        GXDLMSIECOpticalPortSetup optical = new GXDLMSIECOpticalPortSetup();
        optical.setDefaultMode(OpticalProtocolMode.DEFAULT);
        optical.setProposedBaudrate(BaudRate.BAUDRATE_9600);
        optical.setDefaultBaudrate(BaudRate.BAUDRATE_300);
        optical.setResponseTime(LocalPortResponseTime.ms200);
        optical.setDeviceAddress("Gurux");
        optical.setPassword1("Gurux1");
        optical.setPassword2("Gurux2");
        optical.setPassword5("Gurux5");
        getItems().add(optical);
    }

    /*
     * Add Demand Register object.
     */
    void addDemandRegister() {
        java.util.Calendar tm = Calendar.getInstance();
        java.util.Date now = tm.getTime();
        GXDLMSDemandRegister dr = new GXDLMSDemandRegister();
        dr.setLogicalName("1.0.31.4.0.255");
        dr.setCurrentAverageValue(10);
        dr.setLastAverageValue(20);
        dr.setStatus((int) 1);
        dr.setStartTimeCurrent(new GXDateTime(now));
        dr.setCaptureTime(new GXDateTime(now));
        dr.setPeriod(10);
        dr.setNumberOfPeriods(1);
        getItems().add(dr);
    }

    /*
     * Add Register Monitor object.
     */
    void addRegisterMonitor(GXDLMSRegister register) {
        GXDLMSRegisterMonitor rm = new GXDLMSRegisterMonitor();
        rm.setLogicalName("0.0.16.1.0.255");
        rm.setThresholds(null);
        rm.getMonitoredValue().update(register, 2);
        getItems().add(rm);
    }

    /*
     * Add action schedule object.
     */
    void addActionSchedule() {
        // Add Activate test mode Script table object.
        GXDLMSScriptTable st = new GXDLMSScriptTable("0.1.10.1.101.255");
        GXDLMSScript s = new GXDLMSScript();
        s.setId(1);
        GXDLMSScriptAction a = new GXDLMSScriptAction();
        s.getActions().add(a);
        st.getScripts().add(s);
        getItems().add(st);

        GXDLMSActionSchedule actionS = new GXDLMSActionSchedule();
        actionS.setTarget(st);
        actionS.setExecutedScriptSelector(1);
        actionS.setType(SingleActionScheduleType.SingleActionScheduleType1);
        actionS.setExecutionTime(new GXDateTime[]{
            new GXDateTime(Calendar.getInstance().getTime())});
        getItems().add(actionS);
    }

    /*
     * Add EOB Period.
     */
    void addEOBPeriod() {
        // Add Activate test mode Script table object.
        GXDLMSScriptTable st = new GXDLMSScriptTable("0.0.15.0.0.255");
        GXDLMSScript s = new GXDLMSScript();
        s.setId(1);
        GXDLMSScriptAction a = new GXDLMSScriptAction();
        s.getActions().add(a);
        st.getScripts().add(s);
        getItems().add(st);

        GXDLMSActionSchedule actionS = new GXDLMSActionSchedule();
        actionS.setTarget(st);
        actionS.setExecutedScriptSelector(1);
        actionS.setType(SingleActionScheduleType.SingleActionScheduleType1);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 1);
        actionS.setExecutionTime(new GXDateTime[]{
            new GXDateTime(calendar.getTime())});
        getItems().add(actionS);
    }

    /*
     * Add SAP Assignment object.
     */
    void addSapAssignment() {
        GXDLMSSapAssignment sap = new GXDLMSSapAssignment();
        sap.getSapAssignmentList()
                .add(new AbstractMap.SimpleEntry<Integer, String>(1, "Gurux"));
        sap.getSapAssignmentList().add(
                new AbstractMap.SimpleEntry<Integer, String>(16, "Gurux-2"));
        getItems().add(sap);
    }

    /**
     * Add Auto Answer object.
     */
    void addAutoAnswer() {
        GXDLMSAutoAnswer aa = new GXDLMSAutoAnswer();
        aa.setMode(AutoAnswerMode.CALL);
        aa.getListeningWindow()
                .add(new AbstractMap.SimpleEntry<GXDateTime, GXDateTime>(
                        new GXDateTime(-1, -1, -1, 6, -1, -1, -1),
                        new GXDateTime(-1, -1, -1, 8, -1, -1, -1)));
        aa.setStatus(AutoAnswerStatus.INACTIVE);
        aa.setNumberOfCalls(0);
        aa.setNumberOfRingsInListeningWindow(1);
        aa.setNumberOfRingsOutListeningWindow(2);
        getItems().add(aa);
    }

    /*
     * Add Modem Configuration object.
     */
    void addModemConfiguration() {
        GXDLMSModemConfiguration mc = new GXDLMSModemConfiguration();
        mc.setCommunicationSpeed(BaudRate.BAUDRATE_600);
        GXDLMSModemInitialisation init = new GXDLMSModemInitialisation();
        init.setRequest("AT");
        init.setResponse("OK");
        init.setDelay(0);
        mc.setInitialisationStrings(new GXDLMSModemInitialisation[]{init});
        getItems().add(mc);
    }

    /**
     * Add MAC Address Setup object.
     */
    void addMacAddressSetup() {
        GXDLMSMacAddressSetup mac = new GXDLMSMacAddressSetup();
        mac.setMacAddress("11:22:33:44:55:66");
        getItems().add(mac);
    }

    /**
     * Add Image transfer object.
     */
    void addImageTransfer() {
        GXDLMSImageTransfer i = new GXDLMSImageTransfer();
        getItems().add(i);
    }

    /**
     * Add IP4 setup object.
     */
    void addIp4Setup() {
        GXDLMSIp4Setup ip4 = new GXDLMSIp4Setup();
        // Get FIRST local IP address.
        try {
            ip4.setIPAddress(InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            throw new RuntimeException(e.getMessage());
        }
        getItems().add(ip4);
    }

    /**
     * Add Push Setup. (On Connectivity object).
     */
    void addPushSetup() {
        GXDLMSPushSetup push = new GXDLMSPushSetup();
        getItems().add(push);
        // Add push object itself. This is needed to tell structure of data to
        // the Push listener.
        push.getPushObjectList()
                .add(new GXSimpleEntry<GXDLMSObject, GXDLMSCaptureObject>(push,
                        new GXDLMSCaptureObject(2, 0)));
        // Add logical device name.
        GXDLMSObject ldn
                = getItems().findByLN(ObjectType.DATA, "0.0.42.0.0.255");
        push.getPushObjectList()
                .add(new GXSimpleEntry<GXDLMSObject, GXDLMSCaptureObject>(ldn,
                        new GXDLMSCaptureObject(2, 0)));
        // Add .0.0.25.1.0.255 Ch. 0 IPv4 setup IP address.
        GXDLMSObject ip4
                = getItems().findByLN(ObjectType.IP4_SETUP, "0.0.25.1.0.255");
        push.getPushObjectList()
                .add(new GXSimpleEntry<GXDLMSObject, GXDLMSCaptureObject>(ip4,
                        new GXDLMSCaptureObject(3, 0)));
    }

    /**
     * Add GSM Diagnostic.
     */
    void addGSMDiagnostic() {
        GXDLMSGSMDiagnostic gsm = new GXDLMSGSMDiagnostic();
        getItems().add(gsm);
    }

    /**
     * @param port Serial port.
     * @param trace
     * @throws Exception
     */
    public void initialize(String port) throws Exception {
        // If pre-established connections are used.
        setClientSystemTitle("ABCDEFGH".getBytes());
        getCiphering().setSecurity(Security.AUTHENTICATION_ENCRYPTION);
        media = new GXSerial(port, gurux.io.BaudRate.BAUD_RATE_9600, 8,
                Parity.NONE, StopBits.ONE);
        media.setTrace(TraceLevel.VERBOSE);
        media.addListener(this);
        media.open();
        init();
    }

    /**
     * Add available objects.
     *
     * @param server
     */
    public void initialize(int port) {
        // If pre-established connections are used.
        setClientSystemTitle("ABCDEFGH".getBytes());
        getCiphering().setSecurity(Security.AUTHENTICATION_ENCRYPTION);

        media = new GXNet(NetworkType.TCP, port);
        media.setTrace(TraceLevel.VERBOSE);
        media.addListener(this);
        try {
            media.open();
            System.out.println("New meter with logical name DLMS server in port "
                    + String.valueOf(port));
        } catch (Exception ex) {
            System.out.println("Cannot start port " + port + " cause " + ex.toString());
        }
        init();
    }

    @Override
    public void close() throws Exception {
        super.close();
        media.close();
    }

    private boolean needNewLPData() {
        Calendar calendar = Calendar.getInstance();
        long selisih = (calendar.getTimeInMillis() - lastLPCalendar.getTimeInMillis()) / 1000;
        return selisih > 3600;
    }

    private boolean needNewEOBData() {
        Calendar calendar = Calendar.getInstance();
        System.out.println("lastEOBCalendar = " + lastEOBCalendar.getTime());
        if (Calendar.getInstance().get(Calendar.YEAR) > lastEOBCalendar.get(Calendar.YEAR)) {
            return true;
        } else {
            if (Calendar.getInstance().get(Calendar.YEAR) == lastEOBCalendar.get(Calendar.YEAR)
                    && Calendar.getInstance().get(Calendar.MONTH) > lastEOBCalendar.get(Calendar.MONTH)) {
                return true;
            } else {
                return false;
            }
        }
    }

    private void addNewLPData() {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;
        int jumlahLP = getProfileGenericDataCount("1.0.99.1.0.255");
        try {
            reader = new BufferedReader(new FileReader(getDataLPFile()));
            String line;
            int pertama = 1;
            while ((line = reader.readLine()) != null) {
                if (!(jumlahLP == 3000 && pertama == 1)) {
                    sb.append(line);
                    sb.append(System.lineSeparator());
                } else {
                    pertama++;
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TabsDLMSBase.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TabsDLMSBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        SimpleDateFormat df = new SimpleDateFormat();
        while (needNewLPData()) {
            lastLPCalendar.add(Calendar.HOUR_OF_DAY, 1);
            sb.append(df.format(lastLPCalendar.getTime()));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 220 : random.nextInt(110) + 110));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 69 : random.nextInt(100)));
            sb.append(';');
            sb.append(String.valueOf(0));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 1 : random.nextInt(100)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 835 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 123 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 450 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 367 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 532 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 284 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (835 * (lastLPPos + 1)) : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (123 * (lastLPPos + 1)) : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (450 * (lastLPPos + 1)) : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (367 * (lastLPPos + 1)) : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (532 * (lastLPPos + 1)) : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (284 * (lastLPPos + 1)) : random.nextInt(1000)));
            sb.append(System.lineSeparator());
            lastLPPos++;
        }
        synchronized (fileLPLock) {
            FileWriter writer = null;
            try {
                writer = new FileWriter(getDataLPFile(), false);
                writer.write(sb.toString());
                writer.close();
            } catch (IOException e) {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e1) {
                    }
                }
                throw new RuntimeException(e.getMessage());
            }
        }
    }

    private void addNewEOBData() {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;
        int jumlahEOB = getProfileGenericDataCount("1.0.98.1.0.255");
        try {
            reader = new BufferedReader(new FileReader(getDataEOBFile()));
            String line;
            int pertama = 1;
            while ((line = reader.readLine()) != null) {
                if (!(jumlahEOB == 12 && pertama == 1)) {
                    sb.append(line);
                    sb.append(System.lineSeparator());
                } else {
                    pertama++;
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TabsDLMSBase.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TabsDLMSBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        SimpleDateFormat df = new SimpleDateFormat();
        while (needNewEOBData()) {
            lastEOBCalendar.add(Calendar.MONTH, 1);
            sb.append(df.format(lastEOBCalendar.getTime()));
            sb.append(';');
            sb.append(nomorMeter);
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (163 * (lastEOBPos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (223 * (lastEOBPos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 223 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (98 * (lastEOBPos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 98 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (433 * (lastEOBPos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 433 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (256 * (lastEOBPos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 256 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (659 * (lastEOBPos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (251 * (lastEOBPos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (854 * (lastEOBPos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (598 * (lastEOBPos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (111 * (lastEOBPos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (222 * (lastEOBPos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (328 * (lastEOBPos + 1)) : random.nextInt(100000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 744 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 476 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 481 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 397 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 188 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 363 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 444 : random.nextInt(1000)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 11 : random.nextInt(30)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? (12 * (lastEOBPos + 1)) : random.nextInt(30)));
            sb.append(';');
            sb.append(String.valueOf((Long.parseLong(nomorMeter) % 2 == 0) ? 2000 : random.nextInt(10000)));
            sb.append(System.lineSeparator());
            lastEOBPos++;
        }
        synchronized (fileEOBLock) {
            FileWriter writer = null;
            try {
                writer = new FileWriter(getDataEOBFile(), false);
                writer.write(sb.toString());
                writer.close();
            } catch (IOException e) {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e1) {
                    }
                }
                throw new RuntimeException(e.getMessage());
            }
        }
    }

    /**
     * Return data using start and end indexes.
     *
     * @param p ProfileGeneric
     * @param index
     * @param count
     * @return Add data Rows
     */
    private void getProfileGenericDataByEntry(final GXDLMSProfileGeneric p,
            long index, final long count) {
        // Clear old data. It's already serialized.
        p.clearBuffer();
        BufferedReader reader = null;
        SimpleDateFormat df = new SimpleDateFormat();
        if (p.getLogicalName().equals("1.0.99.1.0.255")) {
            synchronized (fileLPLock) {
                try {
                    reader = new BufferedReader(new FileReader(getDataLPFile()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        // Skip row
                        if (index > 0) {
                            --index;
                        } else if (line.length() != 0) {
                            String[] values = line.split("[;]", -1);
                            Object[] objects = new Object[values.length];
                            for (int i = 0; i < objects.length; i++) {
                                if (i == 0) {
                                    objects[i] = df.parse(values[i]);
                                } else {
                                    objects[i] = Integer.parseInt(values[i]);
                                }
                            }
                            p.addRow(objects);
                        }
                        if (p.getBuffer().length == count) {
                            break;
                        }
                    }
                    reader.close();
                } catch (Exception e) {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Exception e1) {
                        }
                    }
                    throw new RuntimeException(e.getMessage());
                }
            }
        } else if (p.getLogicalName().equals("1.0.98.1.0.255")) {
            synchronized (fileEOBLock) {
                try {
                    reader = new BufferedReader(new FileReader(getDataEOBFile()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        // Skip row
                        if (index > 0) {
                            --index;
                        } else if (line.length() != 0) {
                            String[] values = line.split("[;]", -1);
                            Object[] objects = new Object[values.length];
                            for (int i = 0; i < objects.length; i++) {
                                if (i == 0) {
                                    objects[i] = df.parse(values[i]);
                                } else if (i == 1) {
                                    objects[i] = values[i].toString();
                                } else {
                                    objects[i] = Integer.parseInt(values[i]);
                                }
                            }
                            p.addRow(objects);
                        }
                        if (p.getBuffer().length == count) {
                            break;
                        }
                    }
                    reader.close();
                } catch (Exception e) {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Exception e1) {
                        }
                    }
                    throw new RuntimeException(e.getMessage());
                }
            }
        } else {
            synchronized (fileLPLock) {
                try {
                    reader = new BufferedReader(new FileReader(getDataLPFile()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        // Skip row
                        if (index > 0) {
                            --index;
                        } else if (line.length() != 0) {
                            String[] values = line.split("[;]", -1);
                            Object[] objects = new Object[values.length];
                            for (int i = 0; i < objects.length; i++) {
                                if (i == 0) {
                                    objects[i] = df.parse(values[i]);
                                } else {
                                    objects[i] = Integer.parseInt(values[i]);
                                }
                            }
                            p.addRow(objects);
                        }
                        if (p.getBuffer().length == count) {
                            break;
                        }
                    }
                    reader.close();
                } catch (Exception e) {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Exception e1) {
                        }
                    }
                    throw new RuntimeException(e.getMessage());
                }
            }
        }
    }

    /**
     * Find start index and row count using start and end date time.
     *
     * @param start Start time.
     * @param end End time
     * @param index Start index.
     * @param count Item count.
     */
    private void getProfileGenericDataByRange(ValueEventArgs e) {
        List<?> arr = (List<?>) e.getParameters();
        GXDateTime start = (GXDateTime) GXDLMSClient
                .changeType((byte[]) arr.get(1), DataType.DATETIME);
        GXDateTime end = (GXDateTime) GXDLMSClient
                .changeType((byte[]) arr.get(2), DataType.DATETIME);
        System.out.println("start = " + start.toString());
        System.out.println("end = " + end.toString());
        String logicalName = e.getTarget().getLogicalName();
        if (logicalName.equals("1.0.99.1.0.255")) {
            synchronized (fileLPLock) {
                BufferedReader reader = null;
                try {
                    reader = new BufferedReader(new FileReader(getDataLPFile()));
                    String line;
                    SimpleDateFormat df = new SimpleDateFormat();
                    while ((line = reader.readLine()) != null) {
                        String[] values = line.split("[;]", -1);
                        Date tm = df.parse(values[0]);
                        if (tm.compareTo(end.getCalendar().getTime()) > 0) {
                            // If all data is read.
                            break;
                        }
                        if (tm.compareTo(start.getCalendar().getTime()) < 0) {
                            // If we have not find first item.
                            e.setRowBeginIndex(e.getRowBeginIndex() + 1);
                        }
                        e.setRowEndIndex(e.getRowEndIndex() + 1);
                    }
                    reader.close();
                } catch (Exception ex) {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Exception e1) {
                        }
                    }
                    throw new RuntimeException(ex.getMessage());
                }
            }
        } else if (logicalName.equals("1.0.98.1.0.255")) {
            synchronized (fileEOBLock) {
                BufferedReader reader = null;
                try {
                    reader = new BufferedReader(new FileReader(getDataEOBFile()));
                    String line;
                    SimpleDateFormat df = new SimpleDateFormat();
                    while ((line = reader.readLine()) != null) {
                        String[] values = line.split("[;]", -1);
                        Date tm = df.parse(values[0]);
                        if (tm.compareTo(end.getCalendar().getTime()) > 0) {
                            // If all data is read.
                            break;
                        }
                        if (tm.compareTo(start.getCalendar().getTime()) < 0) {
                            // If we have not find first item.
                            e.setRowBeginIndex(e.getRowBeginIndex() + 1);
                        }
                        e.setRowEndIndex(e.getRowEndIndex() + 1);
                    }
                    reader.close();
                } catch (Exception ex) {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Exception e1) {
                        }
                    }
                    throw new RuntimeException(ex.getMessage());
                }
            }
        } else {
            synchronized (fileLPLock) {
                BufferedReader reader = null;
                try {
                    reader = new BufferedReader(new FileReader(getDataEOBFile()));
                    String line;
                    SimpleDateFormat df = new SimpleDateFormat();
                    while ((line = reader.readLine()) != null) {
                        String[] values = line.split("[;]", -1);
                        Date tm = df.parse(values[0]);
                        if (tm.compareTo(end.getCalendar().getTime()) > 0) {
                            // If all data is read.
                            break;
                        }
                        if (tm.compareTo(start.getCalendar().getTime()) < 0) {
                            // If we have not find first item.
                            e.setRowBeginIndex(e.getRowBeginIndex() + 1);
                        }
                        e.setRowEndIndex(e.getRowEndIndex() + 1);
                    }
                    reader.close();
                } catch (Exception ex) {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Exception e1) {
                        }
                    }
                    throw new RuntimeException(ex.getMessage());
                }
            }
        }
    }

    /**
     * Get row count.
     *
     * @return
     */
    private int getProfileGenericDataCount(String logicalName) {
        int rows = 0;
        BufferedReader reader = null;
        if (logicalName.equals("1.0.99.1.0.255")) {
            synchronized (fileLPLock) {
                try {
                    reader = new BufferedReader(new FileReader(getDataLPFile()));
                    while (reader.readLine() != null) {
                        ++rows;
                    }
                    reader.close();
                } catch (Exception e) {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Exception e1) {
                        }
                    }
                    throw new RuntimeException(e.getMessage());
                }
            }
        } else if (logicalName.equals("1.0.98.1.0.255")) {
            synchronized (fileEOBLock) {
                try {
                    reader = new BufferedReader(new FileReader(getDataEOBFile()));
                    while (reader.readLine() != null) {
                        ++rows;
                    }
                    reader.close();
                } catch (Exception e) {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Exception e1) {
                        }
                    }
                    throw new RuntimeException(e.getMessage());
                }
            }
        } else {
            synchronized (fileLPLock) {
                try {
                    reader = new BufferedReader(new FileReader(getDataLPFile()));
                    while (reader.readLine() != null) {
                        ++rows;
                    }
                    reader.close();
                } catch (Exception e) {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Exception e1) {
                        }
                    }
                    throw new RuntimeException(e.getMessage());
                }
            }
        }
        return rows;
    }

    @Override
    public void onPreRead(ValueEventArgs[] args) {
        for (ValueEventArgs e : args) {
            if (e.getTarget().getLogicalName().equals("1.0.99.1.0.255")) {
                if (needNewLPData()) {
                    addNewLPData();
                }
            }
            if (e.getTarget().getLogicalName().equals("1.0.98.1.0.255")) {
                if (needNewEOBData()) {
                    addNewEOBData();
                }
            }
            // Framework will handle Association objects automatically.
            if (e.getTarget() instanceof GXDLMSAssociationLogicalName
                    || e.getTarget() instanceof GXDLMSAssociationShortName
                    || e.getTarget() instanceof GXDLMSIp4Setup) {
                continue;
            }
            if (e.getTarget() instanceof GXDLMSClock && e.getIndex() == 2) {
                GXDLMSClock c = (GXDLMSClock) e.getTarget();
                c.setTime(c.now());
            }
            // Framework will handle profile generic automatically.
            if (e.getTarget() instanceof GXDLMSProfileGeneric) {
                // If buffer is read and we want to save memory.
                if (e.getIndex() == 7) {
                    // If client wants to know EntriesInUse.
                    GXDLMSProfileGeneric p
                            = (GXDLMSProfileGeneric) e.getTarget();
                    p.setEntriesInUse(getProfileGenericDataCount(p.getLogicalName()));
                }
                if (e.getIndex() == 2) {
                    // Client reads buffer.
                    GXDLMSProfileGeneric p
                            = (GXDLMSProfileGeneric) e.getTarget();
                    // Read rows from file.
                    // If reading first time.
                    if (e.getRowEndIndex() == 0) {
                        if (e.getSelector() == 0) {
                            e.setRowEndIndex(getProfileGenericDataCount(p.getLogicalName()));
                        } else if (e.getSelector() == 1) {
                            // Read by entry.
                            getProfileGenericDataByRange(e);
                        } else if (e.getSelector() == 2) {
                            // Read by range.
                            List<?> arr = (List<?>) e.getParameters();
                            e.setRowBeginIndex(((long) arr.get(0)));
                            e.setRowEndIndex(
                                    e.getRowBeginIndex() + (long) arr.get(1));
                            // If client wants to read more data what we have.
                            int cnt = getProfileGenericDataCount(p.getLogicalName());
                            if (e.getRowEndIndex() - e.getRowBeginIndex() > cnt
                                    - e.getRowBeginIndex()) {
                                e.setRowEndIndex(cnt - e.getRowBeginIndex());
                                if (e.getRowEndIndex() < 0) {
                                    e.setRowEndIndex(0);
                                }
                            }
                        }
                    }
                    long count = e.getRowEndIndex() - e.getRowBeginIndex();
                    // Read only rows that can fit to one PDU.
                    if (e.getRowEndIndex() - e.getRowBeginIndex() > e
                            .getRowToPdu()) {
                        count = e.getRowToPdu();
                    }
                    getProfileGenericDataByEntry(p, e.getRowBeginIndex(),
                            count);
                }
                continue;
            }

//            System.out.println(
//                    String.format("Client Read value from %s attribute: %d.",
//                            e.getTarget().getName(), e.getIndex()));
            if ((e.getTarget().getUIDataType(e.getIndex()) == DataType.DATETIME
                    || e.getTarget()
                            .getDataType(e.getIndex()) == DataType.DATETIME)
                    && !(e.getTarget() instanceof GXDLMSClock)) {
                e.setValue(Calendar.getInstance().getTime());
                e.setHandled(true);
            } else if (e.getTarget() instanceof GXDLMSClock) {
                continue;
            } else if (e.getTarget() instanceof GXDLMSRegisterMonitor
                    && e.getIndex() == 2) {
                // Update Register Monitor Thresholds values.
                e.setValue(
                        new Object[]{new java.util.Random().nextInt(1000)});
                e.setHandled(true);
            } else {
                // If data is not assigned and value type is unknown return
                // number.
                Object[] values = e.getTarget().getValues();
                if (e.getIndex() <= values.length) {
                    if (values[e.getIndex() - 1] == null) {
                        DataType tp = e.getTarget().getDataType(e.getIndex());
                        if (tp == DataType.NONE || tp == DataType.INT8
                                || tp == DataType.INT16 || tp == DataType.INT32
                                || tp == DataType.INT64 || tp == DataType.UINT8
                                || tp == DataType.UINT16
                                || tp == DataType.UINT32
                                || tp == DataType.UINT64) {
                            e.setValue(new java.util.Random().nextInt(1000));
                            e.setHandled(true);
                        }
                        if (tp == DataType.STRING) {
                            e.setValue("Tab");
                            e.setHandled(true);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onPostRead(ValueEventArgs[] args) {

    }

    @Override
    public void onPreWrite(ValueEventArgs[] args) {
        for (ValueEventArgs e : args) {
            System.out.println(String.format(
                    "Client Write new value %1$s to object: %2$s.",
                    e.getValue(), e.getTarget().getName()));
        }
    }

    @Override
    public void onPostWrite(ValueEventArgs[] args) {
    }

    @Override
    public void onPreAction(ValueEventArgs[] args) {

    }

    private void handleProfileGenericActions(ValueEventArgs it)
            throws IOException {
//        GXDLMSProfileGeneric pg = (GXDLMSProfileGeneric) it.getTarget();

    }

    @Override
    public void onPostAction(ValueEventArgs[] args) throws Exception {
        for (ValueEventArgs it : args) {
            if (it.getTarget() instanceof GXDLMSProfileGeneric) {
                handleProfileGenericActions(it);
            }
            if (it.getTarget() instanceof GXDLMSSecuritySetup
                    && it.getIndex() == 2) {
                System.out.println(
                        "----------------------------------------------------------");
                System.out.println("Updated keys:");

                System.out.println("Server System title: "
                        + GXCommon.bytesToHex(getCiphering().getSystemTitle()));
                System.out.println("Authentication key: " + GXCommon
                        .bytesToHex(getCiphering().getAuthenticationKey()));
                System.out.println("Block cipher key: " + GXCommon
                        .bytesToHex(getCiphering().getBlockCipherKey()));
                System.out.println("Client System title: "
                        + GXDLMSTranslator.toHex(getClientSystemTitle()));
                System.out.println("Master key (KEK) title: "
                        + GXDLMSTranslator.toHex(getKek()));
            }
        }
    }

    @Override
    public void onError(Object sender, Exception ex) {
        System.out.println("Error has occurred:" + ex.getMessage());
    }

    /*
     * Client has send data.
     */
    @Override
    public void onReceived(Object sender, ReceiveEventArgs e) {
        try {
            synchronized (this) {
                GXServerReply sr = new GXServerReply((byte[]) e.getData());
                do {
                    handleRequest(sr);
                    // Reply is null if we do not want to send any data to the
                    // client.
                    // This is done if client try to make connection with wrong
                    // server or client address.
                    if (sr.getReply() != null) {
                        media.send(sr.getReply(), e.getSenderInfo());
                        sr.setData(null);
                    }
                } while (sr.isStreaming());
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

    @Override
    public void onMediaStateChange(Object sender, MediaStateEventArgs e) {

    }

    /*
     * Client has made connection.
     */
    @Override
    public void onClientConnected(Object sender,
            gurux.net.ConnectionEventArgs e) {
        // Reset server settings when connection is established.
        this.reset();
        System.out.println("Client Connected from " + e.getInfo() + " @" + media.getName());
    }

    /*
     * Client has close connection.
     */
    @Override
    public void onClientDisconnected(Object sender,
            gurux.net.ConnectionEventArgs e) {
        GXNet net = (GXNet) sender;
        System.out.println("Client " + e.getInfo() + " disconnected from port " + media.getName());
    }

    @Override
    public void onTrace(Object sender, TraceEventArgs e) {
        // System.out.println(e.toString());
    }

    @Override
    public void onPropertyChanged(Object sender, PropertyChangedEventArgs e) {

    }

    @Override
    public GXDLMSObject onFindObject(ObjectType objectType, int sn, String ln) {
        return null;
    }

    /**
     * Example server accepts all connections.
     *
     * @param serverAddress Server address.
     * @param clientAddress Client address.
     * @return True.
     */
    @Override
    public final boolean isTarget(final int serverAddress,
            final int clientAddress) {
        return true;
    }

    @Override
    public final SourceDiagnostic onValidateAuthentication(
            final Authentication authentication, final byte[] password) {
        // Accept all passwords.
        // return SourceDiagnostic.NONE;
        // Uncomment checkPassword if you want to check password.
        return checkPassword(authentication, password);
    }

    SourceDiagnostic checkPassword(final Authentication authentication,
            final byte[] password) {
        if (authentication == Authentication.LOW) {

            byte[] expected;
            if (getUseLogicalNameReferencing()) {
                GXDLMSAssociationLogicalName ln
                        = (GXDLMSAssociationLogicalName) getItems().findByLN(
                                ObjectType.ASSOCIATION_LOGICAL_NAME,
                                "0.0.40.0.0.255");
                expected = ln.getSecret();
            } else {
                GXDLMSAssociationShortName sn
                        = (GXDLMSAssociationShortName) getItems().findByLN(
                                ObjectType.ASSOCIATION_SHORT_NAME,
                                "0.0.40.0.0.255");
                expected = sn.getSecret();
            }
            if (java.util.Arrays.equals(expected, password)) {
                return SourceDiagnostic.NONE;
            }
            String actual = "";
            if (password != null) {
                actual = new String(password);
            }
            System.out.println("Password does not match. Actual: '" + actual
                    + "' Expected: '" + new String(expected) + "'");
            return SourceDiagnostic.AUTHENTICATION_FAILURE;
        }
        // Other authentication levels are check on phase two.
        return SourceDiagnostic.NONE;

    }

    @Override
    protected AccessMode onGetAttributeAccess(final ValueEventArgs arg) {
        // Only read is allowed
        if (arg.getSettings().getAuthentication() == Authentication.NONE) {
            return AccessMode.READ;
        }
        // Only clock write is allowed.
        if (arg.getSettings().getAuthentication() == Authentication.LOW) {
            if (arg.getTarget() instanceof GXDLMSClock) {
                return AccessMode.READ_WRITE;
            }
            return AccessMode.READ;
        }
        // All writes are allowed.
        return AccessMode.READ_WRITE;
    }

    @Override
    protected MethodAccessMode onGetMethodAccess(final ValueEventArgs arg) {
        // Methods are not allowed.
        if (arg.getSettings().getAuthentication() == Authentication.NONE) {
            return MethodAccessMode.NO_ACCESS;
        }
        // Only clock methods are allowed.
        if (arg.getSettings().getAuthentication() == Authentication.LOW) {
            if (arg.getTarget() instanceof GXDLMSClock) {
                return MethodAccessMode.ACCESS;
            }
            return MethodAccessMode.NO_ACCESS;
        }
        return MethodAccessMode.ACCESS;
    }

    /**
     * DLMS client connection succeeded.
     */
    @Override
    protected void onConnected(GXDLMSConnectionEventArgs connectionInfo) {
    }

    /**
     * DLMS client connection failed.
     */
    @Override
    protected void
            onInvalidConnection(GXDLMSConnectionEventArgs connectionInfo) {

    }

    /**
     * DLMS client connection closed.
     */
    @Override
    protected void onDisconnected(GXDLMSConnectionEventArgs connectionInfo) {

    }

    /**
     * Schedule or profile generic asks current value.
     *
     * @throws IOException
     */
    @Override
    public void onPreGet(ValueEventArgs[] args) throws IOException {
    }

    /**
     * Schedule or profile generic asks current value.
     */
    @Override
    public void onPostGet(ValueEventArgs[] e) {

    }

    void init() {
        setKek("1111111111111111".getBytes());
        ///////// 1//////////////////////////////////////////////////////////////
        // Add objects of the meter.
        addLogicalDeviceName();
        // Add meter number of the meter.
        addMeterNumber();
        addMeterID1();
        addMeterID2();
        addMeterID3();
        addMeterID4();
        addMeterID5();
        addMeterID6();
        addMeterID7();
        addMeterID8();
        addMeterID9();
        addEOBPeriod();
        // Add firmware version.
        addFirmwareVersion();
        // Add daya aktif import total;
        addDayaAktifImpTot();
        addDayaAktifExpTot();
        addDayaReaktifImpTot();
        addDayaReaktifExpTot();
        addDayaApparentImpTot();
        addDayaApparentExpTot();
        addArusTot();
        addTeganganTot();
        addPowerFactorTot();
        addActivePowerAbsPlusTot();
        addActivePowerAbsMinTot();
        addReactivePowerQ1Tot();
        addReactivePowerQ2Tot();
        addReactivePowerQ3Tot();
        addReactivePowerQ4Tot();
        addActivePowerQ1Tot();
        addActivePowerQ2Tot();
        addActivePowerQ3Tot();
        addActivePowerQ4Tot();
        addTeganganR();
        addArusR();
        addArusN();
        addPFR();
        addDayaAktifImpR();
        addDayaAktifExpR();
        addDayaReaktifImpR();
        addDayaReaktifExpR();
        addDayaApparentImpR();
        addDayaApparentExpR();
        addStandEnergiAktifImpR();
        addStandEnergiAktifExpR();
        addStandEnergiReaktifImpR();
        addStandEnergiReaktifExpR();
        addStandEnergiApparentImpR();
        addStandEnergiApparentExpR();
        addStandEnergiAbsAktifPowerMin();
        addStandEnergiAktifImpTot();
        addPemakaianEnergiAktifImpTot();
        addStandEnergiAktifExpTot();
        addPemakaianEnergiAktifExpTot();
        addStandEnergiReaktifImpTot();
        addPemakaianEnergiReaktifImpTot();
        addStandEnergiReaktifExpTot();
        addPemakaianEnergiReaktifExpTot();
        addStandEnergiReaktifBilling();
        addStandEnergiAktifExpWBP();
        addStandEnergiAktifImpWBP();
        addStandEnergiAktifExpLWBP1();
        addStandEnergiAktifImpLWBP1();
        addStandEnergiAktifExpLWBP2();
        addStandEnergiAktifImpLWBP2();
        addPemakaianEnergiAktifExpWBP();
        addPemakaianEnergiAktifImpWBP();
        addPemakaianEnergiAktifExpLWBP1();
        addPemakaianEnergiAktifImpLWBP1();
        addPemakaianEnergiAktifExpLWBP2();
        addPemakaianEnergiAktifImpLWBP2();
        addMaksApparentExpTotal();
        addTotalKejadianMeterOff();
        addTotalKejadianTamper();
        addKapasitasBaterai();
        // Add invocation counter.
        addInvocationCounter();
        GXDLMSRegister register = addRegister();
        // Add default clock object.
        addClock();
        // Add profile generic object.
//        addProfileGeneric(clock, register);
        // Add load profile object.
        addLoadProfile();
        // Add EOB object.
        addEOB();
        // Add TCP/IP UDP setup object.
        addTcpUdpSetup();
        // Add Auto connect object.
        addAutoConnect();
        // Add Activity Calendar object.
        addActivityCalendar();

        // Add Optical Port Setup object.
        addOpticalPortSetup();

        // Add Demand Register object.
        addDemandRegister();

        // Add Register Monitor object.
        addRegisterMonitor(register);

        // Add action schedule object.
        addActionSchedule();

        // Add SAP Assignment object.
        addSapAssignment();
        // Add Auto Answer object.
        addAutoAnswer();

        // Add Modem Configuration object.
        addModemConfiguration();
        // Add MAC Address Setup object.
        addMacAddressSetup();
        // Add Image transfer object.
        addImageTransfer();

        // Add IP4 Setup object.
        addIp4Setup();

        // Add Push Setup. (On Connectivity object)
        addPushSetup();

        // Add GSM Diagnostic.
        addGSMDiagnostic();

        getItems().add(new GXDLMSCompactData());
        getItems().add(new GXDLMSDisconnectControl());
        ///////////////////////////////////////////////////////////////////////
        // Server must initialize after all objects are added.
        super.initialize();
    }
}
